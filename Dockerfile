```
FROM registry.neppo.com.br/alpine-oraclejdk8:1.0.0
MAINTAINER Neppo "infra@neppo.com.br"

RUN apk add --no-cache tzdata
ENV TZ=America/Sao_Paulo
RUN echo "alias ll='ls -alF'" >> /etc/bash.bashrc

RUN mkdir /projeto-teste

COPY ./projeto-teste/target/teste-jira-jenkins*.jar /teste-jira-jenkins

WORKDIR /teste-jira-jenkins

CMD  java -jar teste-jira-jenkins*.jar
