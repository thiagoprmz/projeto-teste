FROM registry.neppo.com.br/alpine-oraclejdk8:1.0.0
MAINTAINER Neppo "infra@neppo.com.br"

RUN apk add --no-cache tzdata
ENV TZ=America/Sao_Paulo

RUN echo "alias ll='ls -alF'" >> /etc/bash.bashrc

# ARGS
ARG USERNAME=thiago
ARG USER_ID=1000

RUN adduser -D -u $USER_ID -s /bin/bash $USERNAME

# VERSIONS
ENV MAVEN_VERSION 3.5.0

ENV MAVEN_FILE apache-maven-$MAVEN_VERSION

RUN apk --no-cache add  --update \
    libffi-dev \
    make \
    bzip2 \
    g++ \
    bash \
    openssl \
    ca-certificates \
    wget \
    aria2 \
    tar \
    xz 

WORKDIR /opt
RUN wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/$MAVEN_VERSION/$MAVEN_FILE-bin.tar.gz
RUN tar -xvf $MAVEN_FILE-bin.tar.gz
RUN rm $MAVEN_FILE-bin.tar.gz

ENV M2_REPO /m
RUN mkdir $M2_REPO

RUN mkdir /c
RUN chown -R $USERNAME:$USERNAME /c /opt
RUN ln -s $M2_REPO ~/.m2
RUN chown -R $USERNAME:$USERNAME $M2_REPO


USER $USERNAME

ENV PATH $PATH:/opt/$MAVEN_FILE/bin

WORKDIR /c